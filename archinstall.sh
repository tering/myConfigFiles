#!/bin/bash

timedatectl set-ntp true

sed -i "/#Color/s/^#//g" /etc/pacman.conf

cat >> /etc/pacman.conf <<EOF
[archlinuxcn]
Server = https://mirrors.ustc.edu.cn/archlinuxcn/$arch
EOF

# ----------------------------------------------------------------------

reflector -c china --sort rate --save /etc/pacman.d/mirrorlist
pacstrap /mnt base linux linux-firmware python-pynvim neovim zsh iwd base-devel openssh grub efibootmgr
genfstab -U /mnt >> /mnt/etc/fstab
pacman -Syy


set -o errexit
set -o errtrace

# ----------------------------------------------------------------------

ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
hwclock --systohc

sed -i "/#zh_CN.UTF-8/s/^#//g" /etc/locale.gen
sed -i "/#en_US.UTF-8/s/^#//g" /etc/locale.gen
locale-gen
echo LANG=zh_CN.UTF-8 > /etc/locale.conf


export myhostname="PN"
echo ${myhostname} > /etc/hostname
cat >> /etc/hosts <<EOF
127.0.0.1       localhost
::1             localhost
127.0.1.1       ${myhostname}.localdomain    ${myhostname}
EOF

# ----------------------------------------------------------------------

ln -s /usr/bin/nvim /usr/bin/vi
ln -s /usr/bin/nvim /usr/bin/vim
echo "setting root password ..."
passwd
chsh -s /usr/bin/zsh

# ----------------------------------------------------------------------

systemctl enable systemd-networkd systemd-resolved sshd iwd
grub-install --target=x86_64-efi --efi-directory=/boot
grub-mkconfig -o /boot/grub/grub.cfg
#sed -i '/timeout=/s/=./=0/g' /boot/grub/grub.cfg
sed -i "/#Color/s/^#//g" /etc/pacman.conf

mkdir -p ~/gitHome
mkdir -p ~/.config
mkdir -p ~/.ssh
mkdir -p ~/.dwm
mkdir -p ~/GoPath/{bin,pkg,src}

apps=''
apps+='git reflector '
apps+='archlinuxcn-keyring '
apps+='xf86-video-amdgpu mesa mesa-vdpau alsa-utils '
apps+='xorg xorg-apps xorg-server xorg-xinit '
apps+='nerd-fonts-ubuntu-mono noto-fonts-cjk '
apps+='fcitx5-im fcitx5-rime fcitx5-chinese-addons fcitx5-pinyin-zhwiki-rime '
apps+='alacritty '
apps+='htop neofetch '
apps+='picom '
apps+='asp '
apps+='delta '
apps+='rofi '
apps+='feh unzip '
apps+='firefox firefox-i18n-zh-cn '
apps+='visual-studio-code-bin podman '
apps+='dos2unix '
apps+='ranger python-pip '

pacman -Syy
pacman --noconfirm -S $apps

# ----------------------------------------------------------------------

my_email="xgp_tering@163.com"
ssh-keygen -t rsa -C $my_email
git config --global user.name "tering"
git config --global user.email $my_email
git config --global log.date short-local
git config --global core.quotepath false

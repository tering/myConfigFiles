#!/usr/bin/bash

# 安装 oh-my-zsh
omzsh="$HOME/.config/ohmyzsh"
git clone --depth=1 https://ghproxy.com/https://github.com/ohmyzsh/ohmyzsh.git $omzsh
git clone --depth=1 https://ghproxy.com/https://github.com/romkatv/powerlevel10k.git $omzsh/custom/themes/powerlevel10k
git clone --depth=1 https://ghproxy.com/https://github.com/zsh-users/zsh-autosuggestions $omzsh/custom/plugins/zsh-autosuggestions
git clone --depth=1 https://ghproxy.com/https://github.com/zsh-users/zsh-syntax-highlighting $omzsh/custom/plugins/zsh-syntax-highlighting


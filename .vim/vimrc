" 插件管理器：
" curl -fLo ~/.vim/autoload/plug.vim --create-dirs http://ghproxy.com/https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
" curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs http://ghproxy.com/https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
" -----------------------------------------------------------------------------------
" 基本功能配置
" -----------------------------------------------------------------------------------
set mouse-=a                    " 开启鼠标
set nocompatible                " 不兼容 vi
set nu                          " 显示行号
set relativenumber              " 相对行号
set cursorline                  " 光标所在的当前行高亮
set showmode                    " 底部显示当前模式
set showcmd                     " 显示命令
set history=1000                " 保存命令历史记录的条数
set encoding=utf-8              " 使用 utf-8 编码方式
set t_Co=256                    " 使用 256 色
set autoindent                  " 自动缩进
set autowrite                   " 自动写入
set expandtab                   " 由于 Tab 键在不同的编辑器缩进不一致，该设置自动将 Tab 转为空格
set tabstop=4                   " 缩进宽度
set softtabstop=4               " Tab 转为多少个空格
set scrolloff=5                 " 垂直滚动时，光标距离顶部/底部的位置（单位：行）
set showmatch                   " 光标遇到括号时，自动高亮对应的另边
set hlsearch                    " 搜索时，高亮显示匹配结果
set incsearch                   " 输入搜索模式时，每输入一个字符，就自动跳到第一个匹配的结果
set ignorecase                  " 搜索时忽略大小写
set smartcase                   " 智能大小写
set pastetoggle=<F3>            " 粘贴模式

filetype on                     " 根据不同文件启用不同配置
filetype indent on              " 不同文件采用不同缩进
filetype plugin on              " 允许插件
filetype plugin indent on       " 启动自动补全
syntax on                       " 开启代码高亮
au InsertLeave *.go,*.sh write  " 退出插入模式自动保存


" -----------------------------------------------------------------------------------
" 快捷键配置
" -----------------------------------------------------------------------------------
let mapleader=" "               " 设置空格为leader键
nmap <F5> :so $MYVIMRC<CR>
nmap <F3> :noh<CR>

map <leader>w :w<CR>
map <leader>s :wq<CR>
map <leader>q :q!<CR>

map <leader>vim :set splitright<CR>:vs $MYVIMRC<CR>

" 以 root 权限保存文件
map <leader>rt :w !sudo tee % > /dev/null<CR>

" 配置 %% 代表当前目录
cnoremap <expr> %% getcmdtype() == ':'?expand('%:h').'/':'%%'

" 统计上次查找结果的个数
map <leader>/ :%s///gn<CR>

" 执行当前行脚本
map <leader>bh :.w !sh<CR>
" 执行选中的脚本
vmap <leader>bh :w !sh<CR>

" 快速修复bug
map <C-n> :cnext<CR>
map <C-m> :cprevious<CR>
nnoremap <leader>a :cclose<CR>

" -----------------------------------------------------------------------------------
" 插件安装配置
" -----------------------------------------------------------------------------------
call plug#begin('~/.vim/plugged')

" Go 语言相关插件
Plug 'https://ghproxy.com/https://github.com/fatih/vim-go', { 'do': ':GoUpdateBinaries'  }
Plug 'https://ghproxy.com/https://github.com/AndrewRadev/splitjoin.vim'    " 结构体拆分与合并
" Plug 'https://ghproxy.com/https://github.com/ycm-core/YouCompleteMe'       " 自动补全
Plug 'https://ghproxy.com/https://github.com/neoclide/coc.nvim', {'branch': 'release'}

" RUST 语言相关插件
"Plug 'https://ghproxy.com/https://github.com/rust-lang/rust.vim'

" 代码片段
"Plug 'https://ghproxy.com/https://github.com/SirVer/ultisnips'
Plug 'https://ghproxy.com/https://github.com/honza/vim-snippets'

" 代码检索
Plug 'https://ghproxy.com/https://github.com/ctrlpvim/ctrlp.vim'


Plug 'https://ghproxy.com/https://github.com/preservim/nerdtree'
"Plug 'https://ghproxy.com/https://github.com/jistr/vim-nerdtree-tabs'
"Plug 'https://ghproxy.com/https://github.com/Xuyuanp/nerdtree-git-plugin'
"Plug 'https://ghproxy.com/https://github.com/tiagofumo/vim-nerdtree-syntax-highlight'
Plug 'https://ghproxy.com/https://github.com/rafi/awesome-vim-colorschemes'        " 主题套餐
Plug 'https://ghproxy.com/https://github.com/vim-airline/vim-airline'
Plug 'https://ghproxy.com/https://github.com/vim-airline/vim-airline-themes'
Plug 'https://ghproxy.com/https://github.com/junegunn/vim-easy-align'
"Plug 'https://ghproxy.com/https://github.com/mattn/emmet-vim'
"Plug 'https://ghproxy.com/https://github.com/jiangmiao/auto-pairs'

"Plug 'https://ghproxy.com/https://github.com/junegunn/fzf', { 'do': './install --bin'  }
"Plug 'https://ghproxy.com/https://github.com/junegunn/fzf.vim'
"Plug 'https://ghproxy.com/https://github.com/mattn/emmet-vim'
Plug 'https://ghproxy.com/https://github.com/alvan/vim-closetag'
"Plug 'https://ghproxy.com/https://github.com/prettier/vim-prettier', {'do': 'yarn install', 'branch': 'release/0.x'}
"Plug 'https://ghproxy.com/https://github.com/godlygeek/tabular'
"Plug 'https://ghproxy.com/https://github.com/majutsushi/tagbar'
"Plug 'https://ghproxy.com/https://github.com/plasticboy/vim-markdown'
"Plug 'https://ghproxy.com/https://github.com/suan/vim-instant-markdown', {'for': 'markdown'}
"Plug 'https://ghproxy.com/https://github.com/Yggdroot/indentLine'

" 翻译
Plug 'https://ghproxy.com/https://github.com/voldikss/vim-translator'

"Plug 'https://ghproxy.com/https://github.com/ryanoasis/vim-devicons'
call plug#end()

" nerdtree 配置
map <leader>n :NERDTreeToggle<CR>
let NERDTreeMapOpenExpl = ""
let NERDTreeMapUpdir = ""
let NERDTreeMapUpdirKeepOpen = "l"
let NERDTreeMapOpenSplit = ""
let NERDTreeOpenVSplit = ""
let NERDTreeMapActivateNode = "i"
let NERDTreeMapOpenInTab = "o"
let NERDTreeMapPreview = ""
let NERDTreeMapCloseDir = "n"
let NERDTreeMapChangeRoot = "y"

" 配置翻译插件
nmap <silent> <leader>fy <Plug>TranslateW
vmap <silent> <leader>fy <Plug>TranslateWV
nmap <silent> <Leader>fr <Plug>TranslateR
vmap <silent> <Leader>fr <Plug>TranslateRV

" 主题方案
colorscheme space-vim-dark      " 设置配色方案
hi Comment cterm=italic         " 开启斜体（依赖于主题）

" Go 语言快捷键配置
function! s:build_go_files()
  let l:file = expand('%')
  if l:file =~# '^\f\+_test\.go$'
    call go#test#Test(0, 1)
  elseif l:file =~# '^\f\+\.go$'
    call go#cmd#Build(0)
  endif
endfunction

autocmd BufNewFile,BufRead *.go setlocal noexpandtab tabstop=4 shiftwidth=4
autocmd FileType go nmap <leader>b :<C-u>call <SID>build_go_files()<CR>
autocmd FileType go nmap <leader>r <Plug>(go-run)
autocmd FileType go nmap <leader>t <Plug>(go-test)
autocmd FileType go nmap <Leader>c <Plug>(go-coverage-toggle)

let g:go_fmt_command="goimports"
let g:go_highlight_types = 1
let g:go_highlight_fields = 1
let g:go_highlight_functions = 1
let g:go_highlight_function_calls = 1
let g:go_highlight_operators = 1
let g:go_highlight_extra_types = 1
let g:go_highlight_build_constraints = 1
let g:go_highlight_generate_tags = 1

let g:UltiSnipsExpandTrigger="<F4>"
let g:UltiSnipsJumpForwardTrigger="<c-b>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"

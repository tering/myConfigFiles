# ArchLinux

这是一个高度可定制的 Linux 发行版，官方教程相当全面，对于理解 Linux 很有帮助。

- 官方教程：<https://wiki.archlinux.org>

## 安装

强烈建议按照 [官方安装教程](https://wiki.archlinux.org/index.php/Installation_guide_(%E7%AE%80%E4%BD%93%E4%B8%AD%E6%96%87)) 的步骤进行安装

### 安装步骤

1. 磁盘分区并挂载
2. 连网
3. 执行命令👇，下载脚本并运行，开始安装操作系统
    ```shell
    curl -O https://gitee.com/tering/myConfigFiles/raw/master/arch/sh/1-install.sh && ./1-install.sh
    ```
4. 执行命令👇，开始系统的初始化配置
    ```shell
    curl -O https://gitee.com/tering/myConfigFiles/raw/master/arch/sh/2-init.sh && ./2-init.sh
    ```
5. 重启
6. 执行命令👇，安装所需的软件包，完成配置
    ```shell
    curl -O https://gitee.com/tering/myConfigFiles/raw/master/arch/sh/3-app.sh && ./3-app.sh
    ```
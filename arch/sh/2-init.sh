#!/bin/bash

# 发生错误就退出
set -o errexit
set -o errtrace

# ----------------------------------------------------------------------

# 配置时区
ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
# 将时间写入机器
hwclock --systohc
# 配置系统语言
sed -i "/#zh_CN.UTF-8/s/^#//g" /etc/locale.gen
sed -i "/#en_US.UTF-8/s/^#//g" /etc/locale.gen
locale-gen
echo LANG=zh_CN.UTF-8 > /etc/locale.conf
# 设置主机名与网络
export myhostname="XGP-Arch"
echo ${myhostname} > /etc/hostname
cat >> /etc/hosts <<EOF
127.0.0.1       localhost
::1             localhost
127.0.1.1       ${myhostname}.localdomain    ${myhostname}
EOF

# ----------------------------------------------------------------------

# 创建用户
export myuname="tering"
useradd -G wheel -s /bin/zsh $myuname
mkdir /home/$myuname
chown -R tering:tering /home/$myuname
ln -s /usr/bin/vim /usr/bin/vi
visudo
# 设置密码
echo "setting root password ..."
passwd
echo "setting $myuname password ..."
passwd $myuname

# ----------------------------------------------------------------------

# 配置开机启动的服务
systemctl enable systemd-networkd systemd-resolved sshd iwd
# 启动引导
grub-install --target=x86_64-efi --efi-directory=/boot
grub-mkconfig -o /boot/grub/grub.cfg
# 设置跳过开机等待
sed -i '/timeout=/s/=./=0/g' /boot/grub/grub.cfg
# 让 pacman 有颜色
sed -i "/#Color/s/^#//g" /etc/pacman.conf
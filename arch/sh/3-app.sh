#!/bin/bash

# 创建目录结构
mkdir -p ~/gitHome
mkdir -p ~/GoPath/bin
mkdir -p ~/GoPath/pkg
mkdir -p ~/GoPath/src
mkdir -P ~/.config

# 需要安装的软件包
apps='git neofetch yay '
apps+='xf86-video-amdcpu '          # 显卡驱动
apps+='xorg-server xorg-xinit '     # 图形界面
apps+='nerd-fonts-ubuntu-mono '     # 字体
apps+='noto-fonts-cjk '             # 字体
apps+='fcitx5-im fcitx5-rime '      # 输入法框架软件包
apps+='fcitx5-pinyin-zhwiki-rime '  # 词库
apps+='alacritty '  # 终端模拟器
apps+='picom '      # 窗口渲染器：实现透明效果
apps+='rofi '       # 程序启动器
apps+='feh '        # 换壁纸
sudo pacman -Syy
sudo pacman --noconfirm -S $apps

# ----------------------------------------------------------------------

# 生成 ssh 密钥
my_email="xgp_tering@163.com"
ssh-keygen -t rsa -C $my_email
# git 配置
git config --global user.name "tering"
git config --global user.email $my_email
git config --global log.date short-local
git config --global core.quotepath false

# ----------------------------------------------------------------------

# 安装 vim 插件管理器
curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://ghproxy.com/https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

# 安装 oh-my-zsh
omzsh="$HOME/.config/ohmyzsh"
git clone --depth=1 https://gitee.com/mirrors/oh-my-zsh $omzsh
git clone --depth=1 https://gitee.com/romkatv/powerlevel10k.git $omzsh/custom/themes/powerlevel10k
git clone --depth=1 https://ghproxy.com/https://github.com/zsh-users/zsh-autosuggestions $omzsh/custom/plugins/zsh-autosuggestions
git clone --depth=1 https://ghproxy.com/https://github.com/zsh-users/zsh-syntax-highlighting $omzsh/custom/plugins/zsh-syntax-highlighting

# clone 配置以及软件仓库
cd ~/gitHome
git clone git@gitee.com:tering/myConfigFiles cf
git clone git@gitee.com:tering/dwm
cd dwm && sudo make install clean

# ----------------------------------------------------------------------

# 安装我的惯用字体
sudo cp ~/gitHome/cf/font/* /usr/share/fonts/
cd /usr/share/fonts/
sudo mkfontscale
sudo mkfontdir
sudo fc-cache

# 链接配置文件
cd ~/gitHome/cf/arch

for f in `ls home`;do
    if [ -d home/$f ];then
        ln -s ~/gitHome/cf/arch/home/$f ~/$f
    else
        ln home/$f ~/.$f
    fi
done

for f in `ls cfg`;do
    if [ -d cfg/$f ];then
        ln -s ~/gitHome/cf/arch/cfg/$f ~/.config/$f
    else
        ln cfg/$f ~/.config/$f
    fi
done

#!/bin/bash

# 同步时间
timedatectl set-ntp true

# 配置 pacman ----------------------------------------------

# 让 pacman 有颜色
sed -i "/#Color/s/^#//g" /etc/pacman.conf

# 添加 archlinuxcn 的源
cat >> /etc/pacman.conf <<EOF
[archlinuxcn]
SigLevel = Optional TrustedOnly
Server = http://mirrors.163.com/archlinux-cn/\$arch
EOF

# 更换为国内软件源，加速安装。这里选网易的镜像源
echo 'Server = http://mirrors.163.com/archlinux/$repo/os/$arch' > /etc/pacman.d/mirrorlist

# 更新软件列表
pacman -Syy

# -----------------------------------------------------------

# 安装操作系统及软件
pacstrap /mnt base linux-lts linux-firmware vim zsh iwd base-devel openssh archlinuxcn-keyring grub efibootmgr

# 写入分区挂载
genfstab -U /mnt >> /mnt/etc/fstab

# 进入新系统
arch-chroot /mnt

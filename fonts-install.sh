#!/usr/bin/bash

# 发生错误就退出
set -o errexit
set -o errtrace

# 创建目录
#echo "============================= mkdir ============================="
#cd /usr/share/fonts/
cd /usr/share/fonts/tering
#mkdir tering && cd tering

# 下载并解压字体
#echo "=================== download UbuntuMonoNerd ====================="
#curl -OL https://ghproxy.com/https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/UbuntuMono.zip
#unzip UbuntuMono.zip
#rm *Win* *Mono.ttf

# 为字体改名
#echo "============================ rename ============================="
#curl -OL https://ghproxy.com/https://raw.githubusercontent.com/chrissimpkins/fontname.py/master/fontname.py
#pip3 install fonttools
#python3 fontname.py UbuntuMonoNerd *.ttf

# 下载仓耳今楷
#echo "=========================== download ============================"
#curl -OL http://www.tsanger.cn/download/仓耳今楷03-W04.ttf

# 安装字体
echo "============================ install ============================"
mkfontscale
mkfontdir
fc-cache

# 展示结果
echo "============================== end =============================="
fc-list :lang=zh
fc-list | grep Ubuntu
ls -lha /usr/share/fonts/tering/

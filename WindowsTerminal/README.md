# Windows Terminal

## 简介
Windows 下及其好用的终端，由微软提供开源支持。
源码地址：
- [Gitee 地址](https://gitee.com/mirrors/Windows-Terminal)
- [GitHub 地址](https://github.com/microsoft/Terminal)

## 推荐理由
- 简单的配置
- 便捷的标签
- 好看的界面
- 搭配 WSL ( Windows 下的 Linux 子系统 ) 美滋滋

## 下载安装
- 可以直接到 [GitHub](https://github.com/microsoft/terminal/releases) 下载最新的发行版
- 也可以到微软商店里直接搜索安装

## 配置
- 点击 "Settings" 将 profiles.json 文件中的内容直接粘贴即可。
- 我几乎把所有默认配置全写上了，后面都加上了注释，想自定义的可以直接修改自己的配置文件。
- 我还修改了json的校验文件，汉化了部分中文提示，如果你们使用 vscode 这样的现代化编辑器修改配置时或许能看到它们。

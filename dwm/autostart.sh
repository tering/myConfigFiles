#!/usr/bin/bash

# 将本文件夹软连接到用户家目录下
# ln -s ~/cf/dwm ~/

# 修改屏幕缩放
#xrandr --output DisplayPort-0 --dpi 120

# 自启动汉语输入
fcitx5 -d

picom & # 窗口渲染器，用来实现终端透明的

# 设置桌面壁纸
feh --bg-fill "$HOME/cf/img/bg1.jpg"

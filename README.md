# 我的配置文件

- 独立的软件会用单独的文件夹管理，里面有安装教程、配置方法、使用说明。
- ArchLinux 的系统配置较多，所以我写成脚本的形式，方便日后自动化配置

## 目录结构说明

```
├─ arch/		ArchLinux 的安装脚本与配置文件
│  │
│  ├─ cfg/…     要链接到 .config 下的配置文件
│  ├─ home/…    要链接到家目录下的配置文件
│  └─ sh/…      安装与自动配置脚本
│
├─ img/…        图片
├─ ssh-config   放于 `~/.ssh/` 下
│
├─ fonts-install.sh   字体安装脚本
│
└─ WindowsTerminal/…    win10 的终端配置
```

## 链接文件配置

- 解决屏幕撕裂问题
```
ln -s ~/cf/20-amdgpu.conf /etc/X11/xorg.conf.d
```

## 常用 docker 容器的运行命令

- MariaDB
```shell
docker run --name=maria -d -p 3306:3306 -e MYSQL_ROOT_PASSWORD=root -v ~/dkv/maria:/var/lib/mysql --restart=always mariadb:latest
```

- mongo

```shell
docker run --name=mongo -d -p 27017:27017 -v ~/dkv/mongo/db:/data/db -v ~/dkv/mongo/cfg:/data/configdb --restart=always mongo:latest
```

- redis

```shell
docker run --name=redis -d -p 6379:6379 -v ~/dkv/redis:/data --restart=always redis:alpine
```

- nginx

```shell
docker run --name=nginx -d -p 8080:80 -v ${dist}:/usr/share/nginx/html --restart=always nginx:alpine
```

- redis

```shell
docker run --name=maria -d -p 3306:3306 -e MYSQL_ROOT_PASSWORD=root -v ~/dkv/maria:/var/lib/mysql --restart=always mariadb:latest
```
